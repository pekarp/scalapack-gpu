#!/bin/bash -l
# Standard output and error:
##SBATCH -o ./job.out.%j
##SBATCH -e ./job.err.%j
# Initial working directory:
#SBATCH -D ./
# Job name
#SBATCH -J test_scalapack_gpu

#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4
##SBATCH --ntasks=1
#SBATCH --cpus-per-task=1

#SBATCH --constraint="gpu"

# cobra/talos
#SBATCH --gres=gpu:v100:2
##SBATCH --mem=92500 

# raven
##SBATCH --gres=gpu:a100:4
##SBATCH --mem=125000 
##SBATCH --nvmps # for efficient concurrent access to GPUs from multiple processes: only raven

#SBATCH --mail-type=all
##SBATCH --mail-user=userid@example.mpg.de
#SBATCH --time=00:10:00

# verbose
set -v

export BLOCK_SIZE_PILAENV=5120

k=4
N0=10240
N=$((k*N0))
NB=1024

postfix="_scalapack_wrapper_gpu"

#____________________________________________________________

FilenameC="main"
FilenameExe=$FilenameC$postfix

source compile$postfix.sh

#____________________________________________________________
# run

nvidia-smi
nvcc --version

srun $FilenameExe $N $NB

exit $?
