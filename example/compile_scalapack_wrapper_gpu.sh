#!/usr/bin/bash

module purge

if   [[ "$HOSTNAME" == "rav"* ]]; then
	echo "Compiling on raven"
	module load anaconda/2/2019.03
	module load intel/21.2.0  
	module load impi/2021.2
	module load mkl/2021.2
elif   [[ "$HOSTNAME" == "co"* ]] || [[ "$HOSTNAME" == "talos"* ]]; then
	echo "Compiling on cobra/talos"
	module load anaconda/2/2019.03
	module load intel/21.6.0
	module load impi/2021.6
	module load mkl/2022.2
fi

module load cuda/11.2
unset I_MPI_HYDRA_BOOTSTRAP I_MPI_PMI_LIBRARY 

set -v

module list -l

echo CUDA_HOME=$CUDA_HOME
CUDA_LIBS="-L$CUDA_HOME/lib64 -lcublas -lcudart -lrt -lcusolver -Wl,-rpath,$CUDA_HOME/lib64"

echo MKLROOT=$MKLROOT
MKL_LIBS_NO_SCALAPACK="-L$MKL_HOME/lib/intel64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lmkl_blacs_intelmpi_lp64 -lpthread -lm -Wl,-rpath,$MKL_HOME/lib/intel64"

SCALAPACK_HOME=$HOME/soft/ScaLAPACK/test_installation
SCALAPACK_LIBS="-L$SCALAPACK_HOME/lib -lscalapack -Wl,-rpath,$SCALAPACK_HOME/lib"

MKLINCLUDE=$MKLROOT/include
MKLLIBINTEL64=$MKLROOT/lib/intel64

postfix="_scalapack_wrapper_gpu"

filename="main"
filenameExe="main"$postfix

mpiicc -O3 -DUSE_CUDA -DPIN_MATRICES -I$CUDA_HOME/include -I$MKLROOT/include  -c $filename.c -o $filename.o
mpiicc -O3 -I$CUDA_HOME/include -c dgemm$postfix.c -o dgemm$postfix.o
mpiifort $filename.o dgemm$postfix.o -o $filenameExe -nofor-main $MKL_LIBS_NO_SCALAPACK $SCALAPACK_LIBS $CUDA_LIBS
