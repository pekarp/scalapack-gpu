#include <stdio.h>
#include <stdlib.h>

#include <mpi.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include <cublas_v2.h>
#include <cublas_api.h>

cublasHandle_t handle;

#define gpuErrCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      exit(code);
   }
}

#define cublasErrCheck(ans) { cublasAssert((ans), __FILE__, __LINE__); }
inline void cublasAssert(cublasStatus_t code, const char *file, int line)
{
   if (code != CUBLAS_STATUS_SUCCESS) 
   {
      fprintf(stderr,"CUBLASassert: %d %s %d\n", code, file, line);
      exit(code);
   }
}

void createCublasHandle()
    {
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // check number of devices and their properties
    int nDevices;
    cudaError_t err = cudaGetDeviceCount(&nDevices);
    if (err != cudaSuccess) printf("%s\n", cudaGetErrorString(err));
    
    printf("Number of Devices found: %d\n\n", nDevices);
    int i;
    for (i = 0; i < nDevices; i++) {
        struct cudaDeviceProp prop;
        cudaGetDeviceProperties(&prop, i);
        printf("Device Number: %d\n", i);
        printf("  Device name: %s\n", prop.name);
        printf("  Memory Clock Rate (KHz): %d\n",
               prop.memoryClockRate);
        printf("  Memory Bus Width (bits): %d\n",
               prop.memoryBusWidth);
        printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
               2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
        }


    cudaSetDevice(world_rank%nDevices);
    
    cublasErrCheck( cublasCreate(&handle) );
    }

int dgemm_(char *transa, char *transb, int *m_p, int *n_p, int *k_p, double *alpha, double *h_A, int *lda, double *h_B, int *ldb, double *beta, double *h_C, int *ldc)
    {
    int m = *m_p;
    int n = *n_p;
    int k = *k_p;
    
    cublasOperation_t  CUBLAS_OP_A = CUBLAS_OP_N; 
    cublasOperation_t  CUBLAS_OP_B = CUBLAS_OP_N;
    if(*transa == 'T') CUBLAS_OP_A = CUBLAS_OP_T; 
    if(*transb == 'T') CUBLAS_OP_B = CUBLAS_OP_T; 

    printf("starting dgemm wrapper gpu, m=%d, n=%d, k=%d, transa=%c, transb=%c\n", m, n, k, *transa, *transb);
    
    // Allocate 3 arrays on GPU
	double *d_A, *d_B, *d_C;
    gpuErrCheck( cudaMalloc((void**)&d_A, m * k * sizeof(double)) );
	gpuErrCheck( cudaMalloc((void**)&d_B, k * n * sizeof(double)) );
	gpuErrCheck( cudaMalloc((void**)&d_C, m * n * sizeof(double)) );

	// copy the data from CPU to GPU
	gpuErrCheck( cudaMemcpy(d_A, h_A, m * k * sizeof(double), cudaMemcpyHostToDevice) );
	gpuErrCheck( cudaMemcpy(d_B, h_B, k * n * sizeof(double), cudaMemcpyHostToDevice) );

    cublasErrCheck( cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, alpha, d_A, m, d_B, k, beta, d_C, m) ); 

	// copy the data back from GPU to CPU
	gpuErrCheck( cudaMemcpy(h_C, d_C, m * n * sizeof(double), cudaMemcpyDeviceToHost) );

    gpuErrCheck( cudaFree(d_A) );
	gpuErrCheck( cudaFree(d_B) );
	gpuErrCheck( cudaFree(d_C) );
	
    return 0;
    }
