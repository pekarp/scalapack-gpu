# scalapack-gpu

This is a patched version of Netlib's ScaLAPACK that allows to modify the internal blocking size "PILAENV" (PILAENV=32 by default) and make it efficient to use GPUs for PDGEMM operation.

The relevant source file is located in "build/download/scalapack.tgz/scalapack-2.2.0/PBLAS/SRC/pilaenv.f"

## Installation

Modify if necessary and run `myinstall.sh`. In particular, set the correct installation directory `INSTALL_DIR`.


## Usage in your application

An example of how to use patched PDGEMM is located in folder `example`.

Upon linking with MKL one has to exclude MKL's ScaLAPACK from linking line `-lmkl_scalapack`.

`example\dgemm_scalapack_wrapper_gpu.c` shows how to make a wrapper for `dgemm` and to delegate it to `cublasDgemm`.

In the runtime one has to set enviroment variable `BLOCK_SIZE_PILAENV` with its desired value, for example `export BLOCK_SIZE_PILAENV=5120`.