module purge
module load anaconda/2/2019.03

module load intel/21.6.0
module load impi/2021.6
module load mkl/2022.2
#module load cuda/11.2

MPIRUN_COMMDAND=mpirun
unset I_MPI_HYDRA_BOOTSTRAP I_MPI_PMI_LIBRARY
#MPIRUN_COMMDAND="srun -n 1 -p interactive --time=1:00 --mem=1G"

INSTALL_SUBDIR=test_installation_2
INSTALL_DIR=$HOME/soft/ScaLAPACK/$INSTALL_SUBDIR

MPI_DIR=`which mpicc`

echo MPI_DIR=$MPI_DIR

CFLAGS="-I${MKLROOT}/include"
FCFLAGS="-I${MKLROOT}/include/intel64/lp64 -I${MKLROOT}/include"

LDFLAGS_C="-L$MKL_HOME/lib/intel64 -Wl,-rpath,$MKL_HOME/lib/intel64"
LDFLAGS_FC="-nofor-main -L${MKLROOT}/lib/intel64 -Wl,-rpath,$MKL_HOME/lib/intel64"

python2 setup.py --prefix="$INSTALL_DIR" --mpibindir=$MPI_DIR --mpirun="$MPIRUN_COMMDAND" --mpicc=mpiicc --mpif90=mpiifort --ccflags="$CFLAGS" --fcflags="$FCFLAGS" --ldflags_c="$LDFLAGS_C" --ldflags_fc="$LDFLAGS_FC" --lapacklib="-lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl" |& tee -a setup_py_log_$INSTALL_SUBDIR.txt

cp build/download/scalapack.tgz $INSTALL_DIR
